<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
      $data = array();
      $data['head_frontEnd'] = $this->load->view('frontEnd/head_frontEnd', '' , TRUE);
      $data['header_frontEnd'] = $this->load->view('frontEnd/header_frontEnd','',TRUE);
      $data['navbar_frontEnd'] = $this->load->view('frontEnd/navbar_frontEnd','',TRUE);
      $data['slider_frontEnd'] = $this->load->view('frontEnd/slider_frontEnd','',TRUE);
      $data['leftSidebar_frontEnd'] = $this->load->view('frontEnd/leftSidebar_frontEnd','',TRUE);
      $data['featuresItems_frontEnd'] = $this->load->view('frontEnd/featuresItems_frontEnd','',TRUE);
      $data['categoryTab_frontEnd'] = $this->load->view('frontEnd/categoryTab_frontEnd','',TRUE);
      $data['recommendedItems_frotntEnd'] = $this->load->view('frontEnd/recommendedItems_frotntEnd','',TRUE);
      $data['footer_frontEnd'] = $this->load->view('frontEnd/footer_frontEnd','',TRUE);
		$this->load->view('master',$data);
	}
    
    public function accounts() {
      $data = array();
      $data['head_frontEnd'] = $this->load->view('frontEnd/head_frontEnd', '' , TRUE);
      $data['header_frontEnd'] = $this->load->view('frontEnd/header_frontEnd','',TRUE);
      $data['navbar_frontEnd'] = $this->load->view('frontEnd/navbar_frontEnd','',TRUE);
      $data['slider_frontEnd'] = '';
      $data['leftSidebar_frontEnd'] = $this->load->view('frontEnd/leftSidebar_frontEnd','',TRUE);
      $data['featuresItems_frontEnd'] = "<h1 align='center'>Account Page Content Here...</h1>";
      $data['categoryTab_frontEnd'] = $this->load->view('frontEnd/categoryTab_frontEnd','',TRUE);
      $data['recommendedItems_frotntEnd'] = $this->load->view('frontEnd/recommendedItems_frotntEnd','',TRUE);
      $data['footer_frontEnd'] = $this->load->view('frontEnd/footer_frontEnd','',TRUE);
		$this->load->view('master',$data);
    }
    
    public function wishlist() {
      
    }
    
    public function checkout() {
      
    }
    
    public function cart() {
      $data = array();
      $data['head_frontEnd'] = $this->load->view('frontEnd/head_frontEnd', '' , TRUE);
      $data['header_frontEnd'] = $this->load->view('frontEnd/header_frontEnd','',TRUE);
      $data['navbar_frontEnd'] = $this->load->view('frontEnd/navbar_frontEnd','',TRUE);
      $data['slider_frontEnd'] = '';
      $data['leftSidebar_frontEnd'] = '';
      $data['featuresItems_frontEnd'] = '';
      $data['categoryTab_frontEnd'] = '';
      $data['recommendedItems_frotntEnd'] = '';
      $data['login_frontEnd'] = '';
      $data['cart_frontEnd'] = $this->load->view('frontEnd/pages_frontEnd/cart_frontEnd','',TRUE);
      $data['footer_frontEnd'] = $this->load->view('frontEnd/footer_frontEnd','',TRUE);
		$this->load->view('master',$data);
    }
    
    public function login() {
      $data = array();
      $data['head_frontEnd'] = $this->load->view('frontEnd/head_frontEnd', '' , TRUE);
      $data['header_frontEnd'] = $this->load->view('frontEnd/header_frontEnd','',TRUE);
      $data['navbar_frontEnd'] = $this->load->view('frontEnd/navbar_frontEnd','',TRUE);
      $data['slider_frontEnd'] = '';
      $data['leftSidebar_frontEnd'] = '';
      $data['featuresItems_frontEnd'] = '';
      $data['categoryTab_frontEnd'] = '';
      $data['recommendedItems_frotntEnd'] = '';
      $data['login_frontEnd'] = $this->load->view('frontEnd/pages_frontEnd/login_frontEnd','',TRUE);
      $data['footer_frontEnd'] = $this->load->view('frontEnd/footer_frontEnd','',TRUE);
		$this->load->view('master',$data);
    }
    
}

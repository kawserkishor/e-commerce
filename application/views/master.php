
<?= $head_frontEnd; ?>

  <body>
    <?= $header_frontEnd;?><!--/header-->
    <?= $navbar_frontEnd;?><!--/navbar-->

    <?= $slider_frontEnd;?><!--/slider-->
    <?= $login_frontEnd;?>
    <?= $cart_frontEnd;?>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <?= $leftSidebar_frontEnd;?>
          </div>

          <div class="col-sm-9 padding-right">
            <?= $featuresItems_frontEnd;?><!--features_items-->

            <?= $categoryTab_frontEnd;?><!--/category-tab-->

            <?= $recommendedItems_frotntEnd;?><!--/recommended_items-->

          </div>
        </div>
      </div>
    </section>

    <?= $footer_frontEnd;?><!--/Footer-->



    <script src="<?= base_url();?>asset/js/jquery.js"></script>
    <script src="<?= base_url();?>asset/js/bootstrap.min.js"></script>
    <script src="<?= base_url();?>asset/js/jquery.scrollUp.min.js"></script>
    <script src="<?= base_url();?>asset/js/price-range.js"></script>
    <script src="<?= base_url();?>asset/js/jquery.prettyPhoto.js"></script>
    <script src="<?= base_url();?>asset/js/main.js"></script>
  </body>
</html>